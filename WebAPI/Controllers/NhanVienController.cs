﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using static WebAPI.Repository.NhanVienRepository;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NhanVienController : ControllerBase
    {
        protected readonly INhanVienRepository _nhanVienRepository;

        public NhanVienController(INhanVienRepository nhanVienRepository)
        {
            _nhanVienRepository = nhanVienRepository;
        }

        // GET: api/NhanViens/5
        [HttpGet("{id}")]
        public ActionResult<NhanVien> GetNhanVien(int id)
        {
            var nhanVien = _nhanVienRepository.GetById(id);

            if (nhanVien == null)
            {
                return NotFound();
            }

            return nhanVien;
        }

        // GET: api/NhanViens/5
        [HttpGet("GetNhanVienByNQL/{maNQL}")]
        public ActionResult<List<NhanVien>> GetNhanVienByNQL(int maNQL)
        {
            var nhanViens = _nhanVienRepository.GetNhanVienByNQL(maNQL);
            return nhanViens;
        }

        // PUT: api/NhanVien/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutNhanVien(int id, NhanVien nhanVien)
        {
            if (id != nhanVien.ID)
            {
                return BadRequest();
            }

            _nhanVienRepository.(nhanVien).State = EntityState.Modified;

            try
            {
                await _nhanVienRepository.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NhanVienExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/NhanVien
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<NhanVien>> PostNhanVien(NhanVien nhanVien)
        {
            _context.NhanVien.Add(nhanVien);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetNhanVien", new { id = nhanVien.ID }, nhanVien);
        }

        // DELETE: api/NhanVien/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteNhanVien(int id)
        {
            var nhanVien = await _nhanVienRepository.Delete.FindAsync(id);
            if (nhanVien == null)
            {
                return NotFound();
            }

            _context.NhanVien.Remove(nhanVien);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool NhanVienExists(int id)
        {
            return _context.NhanVien.Any(e => e.ID == id);
        }
    }
}
