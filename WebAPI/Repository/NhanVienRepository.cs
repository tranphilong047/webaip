﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Repository
{
    public class NhanVienRepository
    {
        public interface INhanVienRepository : IRepository<NhanVien>
        {
            public List<NhanVien> GetNhanVienByNQL(int maNQL);
            Task SaveChangesAsync();
        }

        public class NhanVienRepository : Repository<NhanVien>, INhanVienRepository
        {
            public NhanVienRepository(DataContext dbContext) : base(dbContext)
            {
            }

            public NhanVien Add(NhanVien obj)
            {
                throw new NotImplementedException();
            }

            public void AddRange(IEnumerable<NhanVien> objs)
            {
                throw new NotImplementedException();
            }

            public void Delete(NhanVien obj)
            {
                throw new NotImplementedException();
            }

            public List<NhanVien> GetNhanVienByNQL(int maNQL)
            {
                List<NhanVien> nhanViens = _dbContext
                    .NhanVien
                    .Where(x => x.QUANLY_ID == maNQL)
                    .ToList();

                return nhanViens;
            }

            NhanVien IRepository<NhanVien>.GetById(int id)
            {
                throw new NotImplementedException();
            }

            IQueryable<NhanVien> IRepository<NhanVien>.Query()
            {
                throw new NotImplementedException();
            }
        }
    }
}
