﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI
{
    public class RepositoryPatternDemoContext
    {
        internal IQueryable<object> Set<T>()
        {
            throw new NotImplementedException();
        }

        internal Task SaveChangesAsync()
        {
            throw new NotImplementedException();
        }

        internal void Update<TEntity>(TEntity entity) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        internal Task AddAsync<TEntity>(TEntity entity) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }
    }
}