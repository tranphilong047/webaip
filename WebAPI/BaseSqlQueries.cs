﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class BaseSqlQueries : IQueries
    {
        protected readonly string _connectionString;
        public BaseSqlQueries(string connectionString)
        {
            _connectionString = connectionString;
        }
    }
}
