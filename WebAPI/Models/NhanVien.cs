﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class NhanVien
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "nvarchar(255)"), Required]
        public string TEN { get; set; }
        public int PHONGBAN_ID { get; set; }
        [ForeignKey("PHONGBAN_ID")]
        public PhongBan PhongBan { get; set; }
        public int QUANLY_ID { get; set; }
    }
}
