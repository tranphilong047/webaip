﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class PhongBan
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "nvarchar(255)"), Required]
        public string TEN_PHONG { get; set; }
        public ICollection<NhanVien> NhanViens { get; set; }
    }
}
