﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();

        //IRepository<T> Repository<T>() where T : class; // BaseEntity;
    }


    public class UnitOfWork : IUnitOfWork
    {
        #region Fields

        private readonly DbContext _context;
        private readonly Dictionary<string, object> _reps;

        #endregion

        #region Constructors

        public UnitOfWork(DbContext context)
        {
            _context = context;
            _reps = new Dictionary<string, object>();

            //_trans = Activator.CreateInstance(typeof(DomainTransaction), _context);
        }

        #endregion

        #region IUnitOfWork Implementation

        #region Transaction

        //public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        //{
        //    _trans.BeginTransaction();
        //}

        //public void Commit()
        //{
        //    _trans.Commit();
        //}

        //public void Rollback()
        //{
        //    _trans.Rollback();
        //}

        #endregion

        public void Dispose()
        {
            // should not dispose _context because it is 1 per request
            //_trans.Rollback();
        }

        //public IRepository<T> Repository<T>() where T : class
        //{
        //    string name = typeof(IRepository<T>).FullName;
        //    if (!_reps.ContainsKey(name))
        //        _reps[name] = new Repository<T>(_context.Set<T>());
        //    return _reps[name] as IRepository<T>;
        //}

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        #endregion
    }
}
