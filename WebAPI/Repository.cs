﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI
{
    public interface IRepository<T> where T : class
    {
        IUnitOfWork UnitOfWork { get; }
        IQueryable<T> Query();

        T GetById(int id);

        T Add(T obj);

        void AddRange(IEnumerable<T> objs);

        void Delete(T obj);

    }

    public class Repository<T> : IRepository<T> where T : class // BaseEntity
    {
        #region Fields

        protected readonly DataContext _dbContext;
        private readonly DbSet<T> _dbSet;

        #endregion

        #region Constructors

        public Repository(DataContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public IUnitOfWork UnitOfWork => _dbContext;

        #endregion

        #region IRepository Implementation

        #region Get & Query

        public virtual IQueryable<T> Query()
        {
            return _dbSet.AsQueryable();
        }

        public virtual T GetById(int id)
        {
            return _dbSet.Find(id);
        }

        #endregion

        #region Add

        public virtual T Add(T obj)
        {
            var result = _dbSet.Add(obj);
            if (result.State == EntityState.Added)
                return result.Entity;
            return null;
        }

        public virtual void AddRange(IEnumerable<T> objs)
        {
            _dbSet.AddRange(objs);
        }

        #endregion

        #region Delete

        public virtual void Delete(T obj)
        {
            _dbSet.Remove(obj);
        }


        #endregion

        #endregion
    }
}
