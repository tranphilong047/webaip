﻿using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Queries
{
    public interface INhanVienQueries : IQueries
    {
        public List<NhanVien> GetNhanVienByNQL(int maNQL);
        public List<NhanVien> GetNhanVienByTenNV(string tennv);
    }

    public class NhanVienQueries : BaseSqlQueries, INhanVienQueries
    {
        public NhanVienQueries(string connectionString) : base(connectionString) { }

        public List<NhanVien> GetNhanVienByNQL(int maNQL)
        {
            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    DynamicParameters parameter = new DynamicParameters();

                    parameter.Add("@maNQL", maNQL);
                    using var rs = conn.QueryMultipleAsync("GetNhanVienByNQL",
                                            parameter, commandType: CommandType.StoredProcedure)
                                        .Result;
                    try
                    {
                        return rs.Read<NhanVien>().ToList();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public List<NhanVien> GetNhanVienByTenNV(string tennv)
        {
            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    DynamicParameters parameter = new DynamicParameters();

                    parameter.Add("@tennv", tennv);
                    using var rs = conn.QueryMultipleAsync("GetNhanVienByTenNV",
                                            parameter, commandType: CommandType.StoredProcedure)
                                        .Result;
                    try
                    {
                        return rs.Read<NhanVien>().ToList();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
