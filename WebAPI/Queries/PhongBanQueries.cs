﻿using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Queries
{
    public interface IPhongBanQueries : IQueries
    {
        public List<PhongBan> GetPhongBanByID(int maphong);
    }
    public class PhongBanQueries : BaseSqlQueries, IPhongBanQueries
    {
        public PhongBanQueries(string connectionString) : base(connectionString) { }

        public List<PhongBan> GetPhongBanByID(int maphong)
        {
            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    DynamicParameters parameter = new DynamicParameters();

                    parameter.Add("@maphong", maphong);
                    using var rs = conn.QueryMultipleAsync("GetPhongBanByID",
                                            parameter, commandType: CommandType.StoredProcedure)
                                        .Result;
                    try
                    {
                        return rs.Read<PhongBan>().ToList();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
