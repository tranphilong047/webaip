﻿using Microsoft.AspNetCore.Mvc;
using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MVC.Controllers
{
    public class NhanVienController : Controller
    {
        public IActionResult Index()
        {
            IEnumerable<mvcNhanVienModel> NhanVienList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("NhanVien").Result;
            NhanVienList = response.Content.ReadAsAsync<IEnumerable<mvcNhanVienModel>>().Result;
            return View(NhanVienList);
        }
        public IActionResult AddOrEdit(int id = 0)
        {
            if(id == 0)
                return View(new mvcNhanVienModel());
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("NhanVien/"+id.ToString()).Result;
                return View(response.Content.ReadAsAsync<mvcNhanVienModel>().Result);
            }
            
        }
        [HttpPost]
        public IActionResult AddOrEdit(mvcNhanVienModel nhanvien)
        {
            if(nhanvien.ID == 0)
            {
                 HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("NhanVien", nhanvien).Result;
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("NhanVien/"+nhanvien.ID, nhanvien).Result;
            }
            return RedirectToAction("Index");
        }
        public IActionResult Delete(int id)
        {
                HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("NhanVien/" + id.ToString()).Result;
                return RedirectToAction("Index");
        }
    }
}
