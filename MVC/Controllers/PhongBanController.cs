﻿using Microsoft.AspNetCore.Mvc;
using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MVC.Controllers
{
    public class PhongBanController : Controller
    {
        public IActionResult Index()
        {

            IEnumerable<mvcPhongBanModel> PhongBanList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("NhanVien").Result;
            PhongBanList = response.Content.ReadAsAsync<IEnumerable<mvcPhongBanModel>>().Result;
            return View(PhongBanList);
        }
    }
}
