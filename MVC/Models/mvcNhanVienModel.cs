﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Models
{
    public class mvcNhanVienModel
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "nvarchar(255)"), Required]
        public string TEN { get; set; }
        public int PHONGBAN_ID { get; set; }
        [ForeignKey("PHONGBAN_ID")]
        public mvcPhongBanModel mvcPhongBanModel { get; set; }
        public int QUANLY_ID { get; set; }
    }
}
